package edu.sjsu.android.googlemapsandsqlite;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.net.Uri;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

// Custom class Content Provider
public class LocationsContentProvider extends ContentProvider {
    public static final String PROVIDER_NAME = "edu.sjsu.android.googlemapsandsqlite.locationsinfo";

    // Uri to operate on locations table. A content provider is indentified by it uri
    public static final Uri URI = Uri.parse("content://" + PROVIDER_NAME + "/locations");

    // Constant to identify the requested operation
    private static final int LOCATIONS = 1;

    private static final UriMatcher uriMatcher;
    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, "locations", LOCATIONS);
        Log.d("MapsActivity", "uriMatcher: " + uriMatcher.toString());
    }

    LocationsDB locationsDB;

    // A callback method which is invoked when the content provider is starting up
    @Override
    public boolean onCreate() {
        locationsDB = new LocationsDB(getContext());
        return false;
    }

    // A callback method which is invoked when insert operation is requested on this content provider
    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {
        long rowID = locationsDB.insert(contentValues);
        Uri _uri = null;
        if(rowID > 0) {
            _uri = ContentUris.withAppendedId(URI, rowID);
            getContext().getContentResolver().notifyChange(_uri, null);
            Log.d("MapsActivity", "insert: " + _uri.toString());
            return _uri;
        } throw new SQLException("Fail to insert a record into " + uri);
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Log.d("MapsActivity", "query: " + uri.toString());
        if(uriMatcher.match(uri) == LOCATIONS) {
            return locationsDB.getAllLocations();
        }
        return null;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }


    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int deleteCondition = 0;
        deleteCondition = locationsDB.deleteAllLocation();
        return deleteCondition;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }
}
