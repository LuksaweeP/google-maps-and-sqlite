package edu.sjsu.android.googlemapsandsqlite;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.CursorLoader;
import androidx.loader.content.Loader;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MainActivity extends FragmentActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    private final LatLng LOCATION_UNIV = new LatLng(37.335371, -121.881050);
    private final LatLng LOCATION_CS = new LatLng(37.333714, -121.881860);
    private GoogleMap map;

    protected void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
        setContentView(R.layout.activity_maps);

        int status = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this);
        if (status != ConnectionResult.SUCCESS) {
            int requestCode = 10;
            Dialog dialog = GoogleApiAvailability.getInstance().getErrorDialog(this, status, requestCode);
            dialog.show();
            Log.d("MapsActivity", "onCreate: Cannot Load map");
        } else {
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
            mapFragment.getMapAsync(this::onMapReady);
            Log.d("MapsActivity", "onCreate: Load map");
            LoaderManager.getInstance(this).initLoader(0,null, (LoaderManager.LoaderCallbacks<Cursor>)this);
        }
    }

    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        //map.moveCamera(CameraUpdateFactory.newLatLng(LOCATION_UNIV));
        //map.addMarker(new MarkerOptions().position(LOCATION_CS).title("Find me here!"));

        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                // Drawing marker on the map
                drawMarker(latLng);

                ContentValues contentValues = new ContentValues();
                contentValues.put(LocationsDB.LATITUDE, latLng.latitude);
                contentValues.put(LocationsDB.LONGTITUDE, latLng.longitude);
                contentValues.put(LocationsDB.ZOOM, map.getCameraPosition().zoom);

                LocationInsertTask locationInsertTask = new LocationInsertTask();
                locationInsertTask.execute(contentValues);
                Log.d("MapsActivity", "add lating: " + contentValues.toString());
                Toast.makeText(getBaseContext(), "Marker is added to the Map", Toast.LENGTH_SHORT).show();
            }
        });

        map.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                map.clear();
                LocationDeleteTask locationDeleteTask = new LocationDeleteTask();
                locationDeleteTask.execute();
                Toast.makeText(getBaseContext(), "All markers are removed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void drawMarker(LatLng latLng){
        // Creating an instance of MarkerOptions
        MarkerOptions markerOptions = new MarkerOptions();

        // Setting latitude and longitude for the marker
        markerOptions.position(latLng);

        // Adding marker on the Google Map
        map.addMarker(markerOptions);
    }

    private class LocationInsertTask extends AsyncTask<ContentValues, Void, Void> {

        @Override
        protected Void doInBackground(ContentValues... contentValues) {
            // Setting the values to insert the clicked location into SQLite database
            getContentResolver().insert(LocationsContentProvider.URI, contentValues[0]);
            return null;
        }
    }

    public class LocationDeleteTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            // Deleting all the locations stored in SQLite database
            getContentResolver().delete(LocationsContentProvider.URI, null, null);
            return null;
        }
    }

    @Override
    public Loader onCreateLoader(int arg0, Bundle arg1) {
        // Uri to the content provider LocationContentProvider
        Uri uri = LocationsContentProvider.URI;
        Log.d("MapsActivity", "onCreateLoader: " );
        return new CursorLoader(this, uri, null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader loader, Cursor cursor){
        Log.d("MapsActivity", "onLoadFinished: " );
        int locationCount = 0;
        double latitude = 0;
        double longtitude = 0;
        float zoom = 0;

        locationCount = cursor.getCount();
        cursor.moveToFirst();
        for(int i =0; i < locationCount; i++) {
            latitude = cursor.getDouble(cursor.getColumnIndex(LocationsDB.LATITUDE));
            longtitude = cursor.getDouble(cursor.getColumnIndex(LocationsDB.LONGTITUDE));
            zoom = cursor.getFloat(cursor.getColumnIndex(LocationsDB.ZOOM));
            LatLng location = new LatLng(latitude, longtitude);
            drawMarker(location);
            cursor.moveToNext();
        }

        if(locationCount > 0) {
            map.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(latitude, longtitude)));
            map.animateCamera(CameraUpdateFactory.zoomTo(zoom));
        }
    }

    public void onLoaderReset(Loader arg0){

    }

    public void onClick_CS(View v){
        map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(LOCATION_CS, 18);
        map.animateCamera(update);
        Log.d("MapsActivity", "onClick_CS");
    }

    public void onClick_Univ(View v){
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(LOCATION_UNIV, 14);
        map.animateCamera(update);
        Log.d("MapsActivity", "onClick_Univ");
    }

    public void onClick_City(View v){
        map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(LOCATION_UNIV, 10);
        map.animateCamera(update);
        Log.d("MapsActivity", "onClick_City");
    }

}