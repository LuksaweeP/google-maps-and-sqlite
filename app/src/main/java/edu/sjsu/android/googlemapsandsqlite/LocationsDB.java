package edu.sjsu.android.googlemapsandsqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

public class LocationsDB extends SQLiteOpenHelper {
    // Database name
    private static String DBNAME = "locationMarkerSQLite";
    // Version
    private static int VERSION = 1;
    // Field 1: primary key
    public static final String ROW_ID = "_id";
    // Field 2: latitude
    public static final String LATITUDE = "lat";
    // Field 3: longitude
    public static final String LONGTITUDE = "lng";
    // Field 4: zoom level of the map
    public static final String ZOOM = "zoom";

    // A constant to store the database name
    private static final String DATABASE_TABLE = "locations";

    // an instance variable for SQLiteDatabase
    private SQLiteDatabase db;

    // Construction
    public LocationsDB(Context context) {
        super(context, DBNAME, null, VERSION);
        Log.d("MapsActivity", "LocationDB: " + context.toString());
        this.db = getWritableDatabase();
    }

    // The callback method invoked when the method getReadable provided that the db does not exist
    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "create table " + DATABASE_TABLE + " ( " +
                        ROW_ID + " integer primary key autoincrement, " +
                        LATITUDE + " double, " +
                        LONGTITUDE + " double, " +
                        ZOOM + " text " + " )";
        db.execSQL(sql);
    }

    // Inser a new location to table location
    public long insert(ContentValues contentValues) {
        long rowID = db.insert(DATABASE_TABLE, null, contentValues);
        Log.d("MapsActivity", "LocationDB insert: " + rowID);
        return rowID;
    }


    // Delete all location from the table
    public int deleteAllLocation() {
        int delCondition = db.delete(DATABASE_TABLE, null, null);
        Log.d("MapsActivity", "LocationDB delCondition: " + delCondition);
        return delCondition;
    }

    // Return all the location from the table
    public Cursor getAllLocations() {
        return db.query(DATABASE_TABLE, new String[]{ROW_ID, LATITUDE, LONGTITUDE, ZOOM}, null, null, null, null, null);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
